<nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="pb-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">
            M&F
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-navbar"
                aria-controls="probootstrap-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="#section-home">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-about">Acerca de Nosotros</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-why-us">Por Qué Nosotros?</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-practicing-areas">Servicios</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-attorneys">Nuestro Equipo</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-notices">Noticias</a></li>
                <li class="nav-item"><a class="nav-link" href="#section-contact">Contacto</a></li>
            </ul>
        </div>
    </div>
</nav>
