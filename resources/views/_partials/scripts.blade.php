<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/slick.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
    $('.show_btn').on('click', function () {
        $('div#myCarousel').empty();
        $id = $(this).attr('id_notice');
        $.ajax({
            url: "http://52.14.94.46/MestreyFontalvoWeb/public/getImages/" + $id,
            type: "GET",
            success: function (result) {
                $result = result[0];
                for (i = 0; i < $result.imagenes.length; i++) {
                    if (i === 0){
                        $('div#myCarousel').append($("<div class=\"carousel-item active\">\n" +
                            "   <img class=\"d-block w-100\" src=\""+$result.imagenes[i].url+"\">\n" +
                            "</div>"));
                    }else{
                        $('div#myCarousel').append($("<div class=\"carousel-item\">\n" +
                            "   <img class=\"d-block w-100\" src=\""+$result.imagenes[i].url+"\">\n" +
                            "</div>"));
                    }
                    $('.modal_title').text($result.noticia.titulo);
                }
            },
            error : function (error) {
                console.error(error.responseText);
            }
        });
    });
</script>
