<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Abogados</title>
    <meta name="author" content="Julio Sarmiento Peña">
    <meta name="description" content="Firma de abogados">
    <meta name="keywords" content="mestre y fontalvo, mestrefontalvo, mestrefontalvo.com, abogados, firma de abogados, barranquilla, leyes, abogados barranquilla">
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600|Montserrat:200,300,400"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/law-icons/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/helpers.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="text/html">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="text/html">
    <style>
        .nav-item {
            color: white;
        }
        .nav-item:hover {
            background: rgba(29, 130, 255, 0.3);
        }
    </style>
</head>
