<!DOCTYPE html>
<html lang="es">
@include('_partials.head')
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">
    @include('_partials.nav')
    @yield('content')
    @include('_partials.footer')
    @include('_partials.scripts')
@if(\Illuminate\Support\Facades\Session::has('success'))
    <script>
        swal("Proceso Completado", "{{\Illuminate\Support\Facades\Session::get('success')}}", "success");
    </script>
    <?php
    \Illuminate\Support\Facades\Session::remove('success');
    ?>
@endif
@if(\Illuminate\Support\Facades\Session::has('error'))
    <script>
        swal("Error", "{{\Illuminate\Support\Facades\Session::get('error')}}", "error");
    </script>
    <?php
    \Illuminate\Support\Facades\Session::remove('error');
    ?>
@endif
</body>
</html>
