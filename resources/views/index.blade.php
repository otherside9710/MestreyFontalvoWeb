@extends('layouts.app')
@section('content')
    <!-- BANNER HOME -->
    <section class="pb_cover_v1 text-left cover-bg-black cover-bg-opacity-4"
             style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(1)->url)}})"
             id="section-home">
        <div class="container">
            <div class="row align-items-center justify-content-end">
                <div class="col-md-6  order-md-1">
                    <h4 class="heading mb-3">{{\App\Http\Controllers\HomeController::getSection(1)->titulo}}</h4>
                    <div class="sub-heading"><p
                            class="mb-5">{{\App\Http\Controllers\HomeController::getSection(1)->descripcion}}</p>
                        <p><a href="#section-about" role="button"
                              class="btn smoothscroll pb_outline-light btn-xl pb_font-13 p-4 rounded-0 pb_letter-spacing-2">
                                Date una vuelta!
                            </a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BANNER HOME -->

    <!-- QUIENEES SOMOS -->
    <section class="pb_section pb_section_v1" data-section="about" id="section-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pr-md-7 pr-sm-0">
                    <br>
                    <h2 class="mt-0 heading-border-top mb-3 font-weight-normal">{{\App\Http\Controllers\HomeController::getSection(2)->titulo}}</h2>
                    <?php
                    $data = \App\Http\Controllers\HomeController::getSection(2)->descripcion;
                    $array = explode('>', $data);
                    ?>
                    @foreach($array as $item)
                        <p style="letter-spacing: 0.05em !important" class="pb_font-14"
                           align="justify">{{$item}}</p>
                    @endforeach
                </div>
                <div class="col-lg-5">
                    <div class="images">
                        <img class="img1 img-fluid" style="height: 430px !important;"
                             src="{{asset('assets/images/logo.png')}}" alt="free Template">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- END QUIENEES SOMOS -->

    <!--CONTACT-->
    <section class="pb_sm_py_cover text-center cover-bg-black cover-bg-opacity-4"
             style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(2)->url)}})">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h2 class="heading mb-3">{{\App\Http\Controllers\HomeController::getSection(3)->titulo}}</h2>
                    <p align="justify"
                       class="sub-heading mb-5 pb_color-light-opacity-8">{{\App\Http\Controllers\HomeController::getSection(3)->descripcion}}</p>
                    <p><a href="#section-contact" role="button"
                          class="btn smoothscroll pb_outline-light p-3 rounded-0 pb_font-13 pb_letter-spacing-2">Hazlo!</a>
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END section -->

    <section class="pb_section" data-section="why-us" id="section-why-us">
        <div class="container">
            <div class="row justify-content-md-center text-center mb-5">
                <div class="col-lg-7">
                    <h2 class="mt-0 heading-border-top font-weight-normal">{{\App\Http\Controllers\HomeController::getSection(4)->titulo}}</h2>
                    <p align="justify">{{\App\Http\Controllers\HomeController::getSection(4)->descripcion}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="images right">
                        <img class="img1 img-fluid" style="height: 470px !important;"
                             src="{{asset('assets/images/logo.png')}}" alt="free Template">
                    </div>
                </div>
                <div class="col-lg-5 pl-md-5 pl-sm-0">
                    <div id="exampleAccordion" class="pb_accordion" data-children=".item">
                        <div class="item">
                            <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion1"
                               aria-expanded="true" aria-controls="exampleAccordion1"
                               class="pb_font-18">{{\App\Http\Controllers\HomeController::getSection(5)->titulo}}</a>
                            <div id="exampleAccordion1" class="collapse show" role="tabpanel">
                                <p align="justify">{{\App\Http\Controllers\HomeController::getSection(5)->descripcion}}
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion2"
                               aria-expanded="false" aria-controls="exampleAccordion2"
                               class="pb_font-18">{{\App\Http\Controllers\HomeController::getSection(6)->titulo}}</a>
                            <div id="exampleAccordion2" class="collapse" role="tabpanel">
                                <p align="justify">{{\App\Http\Controllers\HomeController::getSection(6)->descripcion}}
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion3"
                               aria-expanded="false" aria-controls="exampleAccordion3"
                               class="pb_font-18">{{\App\Http\Controllers\HomeController::getSection(7)->titulo}}
                            </a>
                            <div id="exampleAccordion3" class="collapse" role="tabpanel">
                                <p align="justify">{{\App\Http\Controllers\HomeController::getSection(7)->descripcion}}
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion4"
                               aria-expanded="false" aria-controls="exampleAccordion4"
                               class="pb_font-18">{{\App\Http\Controllers\HomeController::getSection(8)->titulo}}</a>
                            <div id="exampleAccordion4" class="collapse" role="tabpanel">
                                <p align="justify">{{\App\Http\Controllers\HomeController::getSection(8)->descripcion}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- SERVICIOS -->
    <section class="pb_section bg-light pb_bg-half" data-section="practicing-areas" id="section-practicing-areas">
        <div class="container">
            <div class="row justify-content-md-center text-center mb-5">
                <div class="col-lg-7">
                    <h2 class="mt-0 heading-border-top font-weight-normal">{{\App\Http\Controllers\HomeController::getSection(9)->titulo}}</h2>
                    <p align="justify">{{\App\Http\Controllers\HomeController::getSection(9)->descripcion}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="single-item pb_slide_v2">
                        <div>
                            <div class="d-lg-flex d-md-block slide_content">
                                <div class="pb_content-media"
                                     style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(4)->url)}});"></div>
                                <div class="slide_content-text text-center">
                                    <div class="pb_icon_v1"><i class="flaticon text-primary flaticon-law"></i>
                                    </div>
                                    <h3 class="font-weight-normal mt-0 mb-4">{{\App\Http\Controllers\HomeController::getSection(10)->titulo}}</h3>
                                    <?php
                                    $data = \App\Http\Controllers\HomeController::getSection(10)->descripcion;
                                    $array = explode('>', $data);
                                    ?>
                                    @foreach($array as $item)
                                        <p style="letter-spacing: 0.05em !important" class="pb_font-14"
                                           align="justify">{{$item}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="d-lg-flex d-md-block slide_content">
                                <div class="pb_content-media"
                                     style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(5)->url)}});"></div>
                                <div class="slide_content-text text-center">
                                    <div class="pb_icon_v1"><i class="flaticon text-primary flaticon-wallet"></i></div>
                                    <h3 class="font-weight-normal mt-0 mb-4">{{\App\Http\Controllers\HomeController::getSection(11)->titulo}}</h3>
                                    <?php
                                    $data = \App\Http\Controllers\HomeController::getSection(11)->descripcion;
                                    $array = explode('>', $data);
                                    ?>
                                    @foreach($array as $item)
                                        <p style="letter-spacing: 0.05em !important" class="pb_font-14"
                                           align="justify">{{$item}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="d-lg-flex d-md-block slide_content">
                                <div class="pb_content-media"
                                     style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(6)->url)}});"></div>
                                <div class="slide_content-text text-center">
                                    <div class="pb_icon_v1"><i
                                            class="text-primary fa fa-handshake-o fa-5x"></i>
                                    </div>
                                    <h3 class="font-weight-normal mt-0 mb-4">{{\App\Http\Controllers\HomeController::getSection(12)->titulo}}</h3>
                                    <?php
                                    $data = \App\Http\Controllers\HomeController::getSection(12)->descripcion;
                                    $array = explode('>', $data);
                                    ?>
                                    @foreach($array as $item)
                                        <p style="letter-spacing: 0.05em !important" class="pb_font-14"
                                           align="justify">{{$item}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="d-lg-flex d-md-block slide_content">
                                <div class="pb_content-media"
                                     style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(7)->url)}});"></div>
                                <div class="slide_content-text text-center">
                                    <div class="pb_icon_v1"><i class="flaticon text-primary flaticon-courthouse"></i>
                                    </div>
                                    <h3 class="font-weight-normal mt-0 mb-4">{{\App\Http\Controllers\HomeController::getSection(13)->titulo}}</h3>
                                    <?php
                                    $data = \App\Http\Controllers\HomeController::getSection(13)->descripcion;
                                    $array = explode('>', $data);
                                    ?>
                                    @foreach($array as $item)
                                        <p style="letter-spacing: 0.05em !important" class="pb_font-14"
                                           align="justify">{{$item}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="d-lg-flex d-md-block slide_content">
                                <div class="pb_content-media"
                                     style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(8)->url)}});"></div>
                                <div class="slide_content-text text-center">
                                    <div class="pb_icon_v1"><i class="flaticon text-primary flaticon-jury"></i>
                                    </div>
                                    <h3 class="font-weight-normal mt-0 mb-4">{{\App\Http\Controllers\HomeController::getSection(24)->titulo}}</h3>
                                    <?php
                                    $data = \App\Http\Controllers\HomeController::getSection(24)->descripcion;
                                    $array = explode('>', $data);
                                    ?>
                                    @foreach($array as $item)
                                        <p style="letter-spacing: 0.05em !important" class="pb_font-14"
                                           align="justify">{{$item}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->

    <!-- TEAM-->
    <section class="pb_section bg-light bg-image with-overlay" data-section="attorneys" id="section-attorneys"
             style="background-image: url({{asset(\App\Http\Controllers\HomeController::getIMGbyID(3)->url)}}); padding: 4em 0 !important;">
        <div class="container">
            <div class="row justify-content-md-center text-center mb-5">
                <div class="col-lg-7">
                    <h2 class="mt-0 heading-border-top light font-weight-normal text-white">{{\App\Http\Controllers\HomeController::getSection(14)->titulo}}</h2>
                    <p align="justify"
                       class="text-white">{{\App\Http\Controllers\HomeController::getSection(14)->descripcion}}
                    </p>
                </div>
            </div>
            <div class="row">
                @foreach($images as $image)
                    <div class="col-md-3">
                        <div class="card text-center pb_card_v1 mb-4">
                            <img class="card-img-top rounded-circle w-50 mx-auto"
                                 style="height: 190px !important; width:190px !important;" src="{{$image->url}}"
                                 alt="Card image cap">
                            <div class="card-body" style="height: 180px;">
                                <h4 class="card-title mt-0 mb-2" style="height: 50%;">{{$image->title}}</h4>
                                <br>
                                <h6 class="card-subtitle mb-2" style="height: 50%;">{{$image->content}}</h6>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- END TEAM-->


    <section class="pb_section">
        <div class="container">
            <div class="row">
                <div class="col-lg">
                    <div class="media pb_media_v1 mb-5">
                        <div class="icon border border-gray rounded-circle d-flex mr-3 display-4 text-primary"><i
                                class="flaticon flaticon-jury"></i></div>
                        <div class="media-body">
                            <h3 class="mt-0 pb_font-17">{{\App\Http\Controllers\HomeController::getSection(15)->titulo}}</h3>
                            <p style="letter-spacing: -0.99px !important" class="pb_font-14" align="justify">
                                {{\App\Http\Controllers\HomeController::getSection(15)->descripcion}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="media pb_media_v1 mb-5">
                        <div class="icon border border-gray rounded-circle d-flex mr-3 display-4 text-primary"><i
                                class="flaticon flaticon-law"></i></div>
                        <div class="media-body">
                            <h3 class="mt-0 pb_font-17">{{\App\Http\Controllers\HomeController::getSection(16)->titulo}}</h3>
                            <p  style="letter-spacing: -0.99px !important" class="pb_font-14"
                               align="justify">{{\App\Http\Controllers\HomeController::getSection(16)->descripcion}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="media pb_media_v1 mb-5">
                        <div class="icon border border-gray rounded-circle d-flex mr-3 display-4 text-primary"><i
                                class="flaticon flaticon-courthouse"></i></div>
                        <div class="media-body">
                            <h3 class="mt-0 pb_font-17">{{\App\Http\Controllers\HomeController::getSection(17)->titulo}}</h3>
                            <?php
                            $data = \App\Http\Controllers\HomeController::getSection(17)->descripcion;
                            $array = explode(':', $data);
                            ?>
                            <p style="letter-spacing: -0.99px !important" class="pb_font-14"
                               align="justify">{{$array[0]. '.'}}</p>
                            <p style="letter-spacing: -0.99px !important" class="pb_font-14"
                               align="justify">{{$array[1]. '.'}}</p>
                            <p style="letter-spacing: -0.99px !important" class="pb_font-14"
                               align="justify">{{$array[2]}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--MODAL -->

    <!--END MODAL-->

    <!-- NOTICES-->
    <section class="pb_section bg-light" data-section="contact" id="section-notices">
        <div class="container">
            <div class="row justify-content-md-center text-center mb-5">
                <div class="col-lg-7">
                    <h2 class="mt-0 heading-border-top font-weight-normal">{{\App\Http\Controllers\HomeController::getSection(18)->titulo}}</h2>
                    <p>{{\App\Http\Controllers\HomeController::getSection(18)->descripcion}}</p>
                </div>
            </div>
            <div class="row">
                <div class="modal fade id-base" id="modal-base" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title modal_title" id="exampleModalLongTitle"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" id="myCarousel">

                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($notices as $notice)
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="https://www.freeiconspng.com/uploads/news-icon-12.png" class="card-img"
                                     alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{$notice->titulo}}</h5>
                                    <p class="card-text">
                                        {{$notice->descripcion}}
                                    </p>
                                    <br>
                                    @if($notice->img == "S")
                                        <button type="button" class="btn btn-outline-success show_btn"
                                                data-toggle="modal"
                                                data-target="#modal-base"
                                                id_notice="{{$notice->id}}"
                                        >
                                            Ver Fotos
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- END NOTICES-->

    <section class="pb_section" data-section="contact" id="section-contact">
        <div class="container">
            <div class="row justify-content-md-center text-center mb-5">
                <div class="col-lg-7">
                    <h2 class="mt-0 heading-border-top font-weight-normal">{{\App\Http\Controllers\HomeController::getSection(19)->titulo}}</h2>
                    <p>{{\App\Http\Controllers\HomeController::getSection(18)->descripcion}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 pr-md-5 pr-sm-0 mb-4">
                    <form action="#">
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control p-3 rounded-0" id="name">
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control p-3 rounded-0" id="email">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="message">Mensaje</label>
                            <textarea cols="30" rows="10" class="form-control p-3 rounded-0" id="message"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit"
                                   class="btn pb_outline-dark pb_font-13 pb_letter-spacing-2 p-3 rounded-0"
                                   value="Enviar">
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <ul class="pb_contact_details_v1">
                        <li>
                            <span
                                class="text-uppercase">{{\App\Http\Controllers\HomeController::getSection(20)->titulo}}</span>
                            {{\App\Http\Controllers\HomeController::getSection(20)->descripcion}}
                        </li>
                        <li>
                            <span
                                class="text-uppercase">{{\App\Http\Controllers\HomeController::getSection(21)->titulo}}</span>
                            {{\App\Http\Controllers\HomeController::getSection(21)->descripcion}}
                        </li>
                        <li>
                            <span
                                class="text-uppercase">{{\App\Http\Controllers\HomeController::getSection(22)->titulo}}</span>
                            {{\App\Http\Controllers\HomeController::getSection(22)->descripcion}}
                        </li>
                        <li>
                            <span
                                class="text-uppercase">{{\App\Http\Controllers\HomeController::getSection(23)->titulo}}</span>
                            {{\App\Http\Controllers\HomeController::getSection(23)->descripcion}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3916.5607338333934!2d-74.79906988581588!3d10.996491258101475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8ef42d76f1864df1%3A0xc9d45cee6a1371e3!2sCra.+54+%2364-245%2C+Barranquilla%2C+Atl%C3%A1ntico!5e0!3m2!1ses-419!2sco!4v1551305709101"
                    width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>
    <!-- END section -->
@endsection
