<?php

namespace App\Http\Controllers;

use App\Imagenes;
use App\Images;
use App\Notices;
use App\NoticesIMG;
use App\Sections;
use Illuminate\Support\Collection;


class HomeController extends Controller
{
    public function index()
    {
        $images = Images::all();
        $notices = Notices::all();

        return view('index', ['images' => $images, 'notices' => $notices]);
    }


    public static function getSection($id)
    {
        $section = Sections::where('id', $id)->first();
        return $section;
    }

    public static function getImages($id)
    {
        $noticesIMG = NoticesIMG::where('noticia', $id)->get();
        $noticia = Notices::where('id', $id)->first();

        $array = [];
        $collection = null;

        array_push($array, (object)array(
            'noticia' => $noticia,
            'imagenes' => $noticesIMG
        ));

        $collection = new Collection($array);

        return $collection;
    }

    public static function getIMGbyID($id)
    {
        $img = Imagenes::where('id', $id)->first();
        return $img;
    }
}
