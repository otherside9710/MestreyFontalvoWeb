<?php
Route::get('/', 'HomeController@index')->name('home');
Route::get('/getSection/{id}', 'HomeController@getSection')->name('getSection');
Route::get('/getImages/{id}', 'HomeController@getImages')->name('getImages');
